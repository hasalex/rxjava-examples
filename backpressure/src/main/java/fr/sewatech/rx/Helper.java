package fr.sewatech.rx;

import java.lang.management.ManagementFactory;

public class Helper {
    static String message(Object content) {
        long uptime = ManagementFactory.getRuntimeMXBean().getUptime();
        String threadName = Thread.currentThread().getName();
        long usedMemory = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() / 1024 / 1024;

        return String.format("%s - [%s] %s (%s MB)", uptime, threadName, content, usedMemory);
    }

    static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}

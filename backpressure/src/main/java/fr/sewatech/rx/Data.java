package fr.sewatech.rx;

import static fr.sewatech.rx.Helper.message;
import static fr.sewatech.rx.Helper.sleep;

class Data {
    final Byte[] bytes;
    final Integer value;

    Data(Integer value) {
        this.value = value;
        System.out.println(message("New data: " + value));
        bytes = new Byte[50_000];
        sleep(10);
    }

    @Override
    public String toString() {
        return "Handled: " + value;
    }
}

package fr.sewatech.rx;

import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.concurrent.Semaphore;

import static fr.sewatech.rx.Helper.message;

class AppLifecycle {

    private final Semaphore sem = new Semaphore(0);

    void letsGo() {
        // Avoid waiting for end of thread pool, esp. with Maven
        System.setProperty("rx3.purge-enabled", "false");
        // Handle errors, esp OOME
        RxJavaPlugins.setErrorHandler(throwable -> Schedulers.shutdown());
    }

    void waitUntilTheEnd() {
        try {
            sem.acquire();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    void thisIsTheEnd() {
        System.out.println(message("Completed"));
        sem.release();
    }
}
